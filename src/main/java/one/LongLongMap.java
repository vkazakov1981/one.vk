package one;

import sun.misc.Unsafe;

/**
 * Требуется написать LongLongMap который по произвольному long ключу хранить произвольное long значение
 * Важно: все данные (в том числе дополнительные, если их размер зависит от числа элементов) требуется хранить в выделенном заранее блоке в разделяемой памяти, адрес и размер которого передается в конструкторе
 * для доступа к памяти напрямую необходимо (и достаточно) использовать следующие два метода:
 * sun.misc.Unsafe.getLong(long), sun.misc.Unsafe.putLong(long, long)
 */
public class LongLongMap {
    private static final long LONG_SIZE = 8;

    interface SystemOffsets {
        long TAIL = 0;
        long BUCKETS_COUNT = TAIL + LONG_SIZE;
        long FINAL = BUCKETS_COUNT + LONG_SIZE;
    }

    ;

    interface KeyValueNextOffsets {
        long KEY = 0;
        long VALUE = KEY + LONG_SIZE;
        long NEXT = VALUE + LONG_SIZE;
        long FINAL = NEXT + LONG_SIZE;
    }

    ;
    private final Unsafe unsafe;
    private final long address;
    private final long size;

    /**
     * @param unsafe  для доступа к памяти
     * @param address адрес начала выделенной области памяти
     * @param size    размер выделенной области в байтах (~100GB)
     */
    LongLongMap(Unsafe unsafe, long address, long size) {
        this.unsafe = unsafe;
        this.address = address;
        this.size = size - size % LONG_SIZE;

        long sizeForBuckets = this.size >> 1; //50% of memory located for buckets
        long numberOfBuckets = sizeForBuckets / LONG_SIZE;

        putTailPtr(SystemOffsets.FINAL + sizeForBuckets);
        putBucketCount(numberOfBuckets);

        //clear buckets
        unsafe.setMemory(address + SystemOffsets.FINAL, sizeForBuckets, (byte) 0);
    }

    /**
     * Метод должен работать со сложностью O(1) при отсутствии коллизий, но может деградировать при их появлении
     *
     * @param k произвольный ключ
     * @param v произвольное значение
     * @return предыдущее значение или 0
     */
    long put(long k, long v) {
        long bucketOffset = getBucketOffset(k);
        long bucketLink = getLong(bucketOffset);
        if (bucketLink == 0) {
            long ptr = putNewKeyValue(k, v);
            putLong(bucketOffset, ptr);
            return 0;
        } else {
            return replaceNewValueOrAddToTail(bucketLink, k, v);
        }
    }

    /**
     * Метод должен работать со сложностью O(1) при отсутствии коллизий, но может деградировать при их появлении
     *
     * @param k ключ
     * @return значение или 0
     */
    long get(long k) {
        long bucketOffset = getBucketOffset(k);
        long bucketLink = getLong(bucketOffset);
        if (bucketLink == 0) {
            return 0;
        } else {
            return findValue(bucketLink, k);
        }
    }

    private long findValue(long keyValueNextOffset, long key) {
        Node node = getNode(keyValueNextOffset);
        if (node.getKey() == key) {
            return node.getValue();
        } else if (node.getNext() == 0) {
            return 0;
        } else {
            return findValue(node.getNext(), key);
        }
    }

    private long putNewKeyValue(long key, long value) {
        long tailOffset = getLong(SystemOffsets.TAIL);
        if (tailOffset >= size - KeyValueNextOffsets.FINAL) {
            throw new RuntimeException("Memory usage threshold exceeded");
        }
        putLong(tailOffset + KeyValueNextOffsets.KEY, key);
        putLong(tailOffset + KeyValueNextOffsets.VALUE, value);
        putLong(tailOffset + KeyValueNextOffsets.NEXT, 0);
        putTailPtr(tailOffset + KeyValueNextOffsets.FINAL);
        return tailOffset;
    }

    private long replaceNewValueOrAddToTail(long nodeOffset, long key, long value) {
        Node node = getNode(nodeOffset);
        if (node.getKey() == key) {
            Node newNode = new Node(node.getKey(), value, node.getNext());
            updateNode(nodeOffset, newNode);
            return node.getValue();
        } else if (node.getNext() == 0) {
            long newTailInList = putNewKeyValue(key, value);
            putLong(nodeOffset + KeyValueNextOffsets.NEXT, newTailInList);
            return 0;
        } else {
            return replaceNewValueOrAddToTail(node.getNext(), key, value);
        }
    }

    private void putLong(long offset, long value) {
        unsafe.putLong(address + offset, value);
    }

    private long getLong(long offset) {
        return unsafe.getLong(address + offset);
    }

    private void putTailPtr(long offset) {
        putLong(SystemOffsets.TAIL, offset);
    }

    private long getBucketCount() {
        return getLong(SystemOffsets.BUCKETS_COUNT);
    }

    private void putBucketCount(long bucketCount) {
        putLong(SystemOffsets.BUCKETS_COUNT, bucketCount);
    }

    private long calculateBucketNumber(long key) {
        return key % getBucketCount();
    }

    private long getBucketOffset(long key) {
        long bucketNumber = calculateBucketNumber(key);
        return SystemOffsets.FINAL + bucketNumber * 8;
    }

    private Node getNode(long offset) {
        long key = getLong(offset + KeyValueNextOffsets.KEY);
        long value = getLong(offset + KeyValueNextOffsets.VALUE);
        long next = getLong(offset + KeyValueNextOffsets.NEXT);
        return new Node(key, value, next);
    }
    private void updateNode(long offset, Node node) {
        putLong(offset + KeyValueNextOffsets.KEY, node.getKey());
        putLong(offset + KeyValueNextOffsets.VALUE, node.getValue());
        putLong(offset + KeyValueNextOffsets.NEXT, node.getNext());
    }

    static class Node {
        private final long key;
        private final long value;
        private final long next;
        Node(long key, long value, long next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public long getKey() {
            return key;
        }

        public long getValue() {
            return value;
        }

        public long getNext() {
            return next;
        }
    }
}
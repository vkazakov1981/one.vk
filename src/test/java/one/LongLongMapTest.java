package one;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class LongLongMapTest {
    private final static long MEMORY_SIZE = 100000000;
    private static Unsafe unsafe = null;
    private static long address;

    @BeforeClass
    public static void init() throws Exception {
        Class c = LongLongMapTest.class.getClassLoader().loadClass("sun.misc.Unsafe");
        Field f = c.getDeclaredField("theUnsafe");
        f.setAccessible(true);
        unsafe = (Unsafe)f.get(c);
        address = unsafe.allocateMemory(MEMORY_SIZE);
    }

    @Test
    public void testBasicPutAndGet() throws Exception {
        LongLongMap map = new LongLongMap(unsafe, address, MEMORY_SIZE);
        long placed1 = map.put(100, 100);
        long placed2 = map.put(200, 150);

        Assert.assertEquals(placed1, 0);
        Assert.assertEquals(placed2, 0);

        long get1 = map.get(100);
        long get2 = map.get(200);
        long getUnexisting = map.get(101);

        Assert.assertEquals(get1, 100);
        Assert.assertEquals(get2, 150);
        Assert.assertEquals(getUnexisting, 0);
    }

    @Test
    public void testHugeMap() throws Exception {
        LongLongMap map = new LongLongMap(unsafe, address, MEMORY_SIZE);
        Map<Long, Long> generatedMap = generateMap(1000000, 0);
        generatedMap.forEach(map::put);

        //lets compare all elements in generatedMap & ourMap
        for(Map.Entry<Long, Long> entry : generatedMap.entrySet()) {
            long value = map.get(entry.getKey());
            Assert.assertEquals(Long.valueOf(value), entry.getValue());
        }
    }

    @Test
    public void testHugeMapWithReplace() throws Exception {
        LongLongMap map = new LongLongMap(unsafe, address, MEMORY_SIZE);
        Map<Long, Long> generatedMap = generateMap(1000000, 0);
        generatedMap.forEach(map::put);

        Map<Long, Long> withShiftMap = generateMap(1000000, 1);
        withShiftMap.forEach(map::put);

        //lets compare all elements in generatedMap & ourMap
        for(Map.Entry<Long, Long> entry : withShiftMap.entrySet()) {
            long value = map.get(entry.getKey());
            Assert.assertEquals(Long.valueOf(value), entry.getValue());
        }
    }

    private Map<Long, Long> generateMap(int size, int shift) {
        Map<Long, Long> map = new HashMap<Long, Long>(size);
        for(long i = 1; i < size; i++) {
            map.put(i, i + shift);
        }
        return map;
    }
}
